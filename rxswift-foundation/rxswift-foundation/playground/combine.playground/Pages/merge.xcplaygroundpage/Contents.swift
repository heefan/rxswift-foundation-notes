import RxSwift

/*:
> merge
 Combines elements from source `Observable` sequences into a single new `Observable` sequence,
 and will emit each element as it is emitted by each source `Observable` sequence.
 
 [More info](http://reactivex.io/documentation/operators/combinelatest.html)
 ![](https://raw.githubusercontent.com/kzaher/rxswiftcontent/master/MarbleDiagrams/png/combinelatest.png)
*/


example("merge") {
    let disposeBag = DisposeBag()
    
    let subject1 = PublishSubject<String>()
    let subject2 = PublishSubject<String>()
    
    Observable.of(subject1, subject2)
        .merge()
        .subscribe(onNext: { print($0) })
        .disposed(by: disposeBag)
    
    subject1.onNext("🅰️")
    subject1.onNext("🅱️")
    subject2.onNext("①")
    subject2.onNext("②")
    subject1.onNext("🆎")
    subject2.onNext("③")
}
