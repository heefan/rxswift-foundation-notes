
import RxSwift

/*:
 ----
 ## `switchLatest`
 
*/

example("switchLatest") {
    let disposeBag = DisposeBag()
    let subject1 = BehaviorSubject(value: "⚽️")
    let subject2 = BehaviorSubject(value: "🍎")
    
    let subjectsSubject = BehaviorSubject(value: subject1)
    
    subjectsSubject.asObservable()
        .switchLatest()
        .subscribe(onNext: { print($0) })
        .disposed(by: disposeBag)
    
    subject1.onNext("🏈")
    subject1.onNext("🏀")
    
    subjectsSubject.onNext(subject2)
    
    subject1.onNext("⚾️")
    
    subject2.onNext("🍐")
}
