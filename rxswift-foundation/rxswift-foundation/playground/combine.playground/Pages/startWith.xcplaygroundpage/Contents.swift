import RxSwift

/*:
 # Combination Operators
 > Operators that combine multiple source `Observable` into single `Observable`.
 - - -
 
 ## `startWith`
> emit a specified sequence of items before beginning to emit the items from the source Observable

*/

example("startWith") {
    let disposeBag = DisposeBag()
    
    Observable.of("🐶", "🐱", "🐭", "🐹")    // The sequenece
        .startWith("1️⃣")   // insert at the beginning of the sequence, still the sequence
        .startWith("2️⃣")   // insert again,  still the sequence
        .startWith("3️⃣", "🅰️", "🅱️") // insert again, still the sequence
        .subscribe(onNext: { print($0) })
        .disposed(by: disposeBag)
    
    // it should print "3️⃣", "🅰️", "🅱️", "2️⃣","1️⃣", "🐶", "🐱", "🐭", "🐹"
}
