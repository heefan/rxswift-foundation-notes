import RxSwift

/*:
 ----
 ## `zip`
 Combines up to 8 source `Observable` sequences into a single new `Observable` sequence, and will emit from the combined `Observable` sequence the elements from each of the source `Observable` sequences at the corresponding index. [More info](http://reactivex.io/documentation/operators/zip.html)
 ![](https://raw.githubusercontent.com/kzaher/rxswiftcontent/master/MarbleDiagrams/png/zip.png)
*/
example("zip") {
    let disposebag = DisposeBag()
    
    let stringSubject = PublishSubject<String>()
    let intSubject = PublishSubject<Int>()
    
    Observable.zip(stringSubject, intSubject) { stringElement, intElement in
        "\(stringElement) \(intElement)"
        }
        .subscribe(onNext: {print($0)})
        .disposed(by: disposebag)
    
    stringSubject.onNext("🅰️")
    stringSubject.onNext("🅱️")
    intSubject.onNext(1)
    intSubject.onNext(2)
    stringSubject.onNext("🆎")
    intSubject.onNext(3)
}

// print
// A1, B2, AB3
