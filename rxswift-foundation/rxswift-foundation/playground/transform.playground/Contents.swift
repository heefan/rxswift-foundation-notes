import RxSwift

/*:
 # `toArray`
 http://reactivex.io/documentation/operators/to.html
 
 see the marbal diagram in `to.c.png`
 
 ## Keep in Mind
 > [Keywords] `Single`, `Complete Event`
 1. toArray() return `Single<[Element]>`, meaning, it only receive on `Success` and `Error`.
 2. if use observable, it will one time create all the elements and emit success event.
 3. if use subject, it will emit element explicitly and emit complete manually.

 */

example("example of toArray Observable") {
    let disposeBag = DisposeBag()

    Observable.of("A", "B", "C").toArray().debug().subscribe{ print($0) }
        .disposed(by: disposeBag)
}

example("example of toArray on PublishSubject") {
    let disposeBag = DisposeBag()
    let subject = PublishSubject<String>()
 
    subject.toArray().debug().subscribe{ print($0) }
        .disposed(by: disposeBag)
    
    subject.onNext("A")
    subject.onNext("B")
    subject.onNext("C")
    subject.onCompleted()   // if do not call onComplete, it will not print anything, coz it is waiting the complete (success) event.
}
